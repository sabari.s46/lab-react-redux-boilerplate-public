import { Provider } from "react-redux";
import { createStore } from "redux";
import reducer from "./redux/Reducer";
import LikeCounter from "./redux/LikeCounter";
import React from "react";

const store = createStore(reducer);

export default function App() {
  return (
    <Provider store={store}>
      <LikeCounter />
    </Provider>
  );
}