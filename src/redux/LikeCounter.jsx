import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { incrementLike, decrementLike } from "./Actions";
import "../App.css";

function LikeCounter() {
  const dispatch = useDispatch();
  const count = useSelector((state) => state.Like);

  const handleIncreaseClick = () => {
    dispatch(incrementLike());
  };

  const handleDecreaseClick = () => {
    dispatch(decrementLike());
  };

  return (
    <div className="boss">
      <div className="display">
        <p>Count: {count}</p>
        <button onClick={handleDecreaseClick}>Decrease</button>
        <button onClick={handleIncreaseClick}>Increase</button>
      </div>
    </div>
  );
}

export default LikeCounter;
