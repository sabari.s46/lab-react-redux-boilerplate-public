const INCREMENT = "INCREMENT";
const DECREMENT = "DECREMENT";

function incrementLike() {
  return { type: INCREMENT, info: "This will increment the like" };
}

function decrementLike() {
  return { type: DECREMENT, info: "This will decrement the like" };
}

export { incrementLike, decrementLike, INCREMENT, DECREMENT };
