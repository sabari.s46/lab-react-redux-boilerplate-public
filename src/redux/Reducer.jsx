import { INCREMENT, DECREMENT } from "./Actions";
const initialState = {
  Like: 0,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case INCREMENT:
      return { ...state, Like: state.Like + 1 };
    case DECREMENT:
      return { ...state, Like: state.Like - 1 };
    default:
      return state;
  }
};

export default reducer;
